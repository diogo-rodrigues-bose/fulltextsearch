import model.Text;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import services.FTService;


public class FTServiceTest {

    private FTService service;

    @Before
    public void initialize() {
        service = new FTService();
    }

    @Test
    public void testInsertText(){
        Assert.assertNotNull(service.findText("Hello"));
    }

    @Test
    public void testGetSynonym(){
        service.addText(new Text("Hello"));
        service.addSynonymsToWord("Goodbye", "Adeus");
        Assert.assertNotNull(service.findFuzzy("Adeus"));
    }

}
