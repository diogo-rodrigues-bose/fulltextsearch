package model;

import java.util.LinkedList;
import java.util.List;

public class Text {

    private String text;

    private Text parent;

    private List<Text> children = new LinkedList<>();
    private int ranking;

    public Text(String text) {
        this.text = text;
    }

    public Text(String text, int ranking) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Text getParent() {
        return parent;
    }

    public void setParent(Text parent) {
        this.parent = parent;
    }

    public List<Text> getChildren() {
        return children;
    }

    public void setChildren(List<Text> children) {
        this.children = children;
    }

    public int getRanking() {
        return (int)(Math.random() * 10);
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }
}
