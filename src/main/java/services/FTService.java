package services;

import model.Text;

import java.util.Arrays;
import java.util.List;


public class FTService {

    /**
     * This method add a text to be indexed along with a ranking to influence it's position. <br/>
     * * EXAMPLE: <br/>
     *      * <i> insertText("I love barbecue")</i> <br/>
     */
    public void addText(Text text) {
    }

    /**
     * This method performs a FTS on the indexed terms. <br/>
     * <p>
     * EXAMPLE: <br/>
     * <i> TEXTS = [ {"I love Barbecue", ranking:3 }, { "I hate fish", ranking: 10}, {"I like chocolate bars", ranking: 12} )</i> <br/>
     * <br/>
     * <i> find("bar") : ["I like chocolate bars", "I love Barbecue"] </i> <br/>
     *
     * @return at max 5 matches with ranking into consideration. <br/>
     */
    public List<Text> findText(String partialText) {
        return Arrays.asList(new Text(partialText));
    }

    /**
     * This method performs a FTS with Fuzziness factor of 1 - ignores a non matching character <br/>
     * Changing a character (box → fox) <br/>
     * Removing a character (black → lack) <br/>
     * Inserting a character (sic → sick) <br/>
     * <p>
     * EXAMPLE:
     * <i> TEXTS = [ {"I love barbecue", ranking:3 }, { "I hate fish", ranking: 10}, {"I like chocolate bars", ranking: 12} )</i>
     * <i> find("bar") : ["I like chocolate bars", "I love barbecue"] </i>
     *
     * @return at max 5 matches with ranking into consideration.
     */
    public List<Text> findFuzzy(String partialText) {
        return Arrays.asList(new Text(partialText));
    }

    /**
     * This method adds synonyms to text.
     * Inserts the text if it does not exist. Updates the synonyms if they already exist <br/>
     * EXAMPLE: <br/>
     * <i> TEXTS = [ {"I love barbecue", ranking:3 }, { "I hate fish", ranking: 10}, {"I like chocolate bars", ranking: 12} )</i> <br/>
     * <i> addSynonymsToWord("barbecue", "food") </i> <br/>
     * <i> addSynonymsToWord("fish", "food") </i> <br/>
     * <i> find("food") : [ "I hate fish", "I love barbecue"] </i> <br/>
     */
    public void addSynonymsToWord(String text, String... synonyms) {
    }


}
